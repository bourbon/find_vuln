from binaryninja import BinaryView, PluginCommand, Symbol, SymbolType

from .memory import process_mem_symbols
from .strings import process_string_symbols
from .util import log_symbols_basic_info


bv = None


def is_imported_symbol(symbol: Symbol):
    return symbol.type in [SymbolType.LibraryFunctionSymbol, SymbolType.ImportedFunctionSymbol]


def find_vulns(bv_: BinaryView):
    global bv
    bv = bv_

    all_symbols = bv.get_symbols()
    imported_symbols = sorted(filter(is_imported_symbol, all_symbols), key=lambda sym: sym.type)
    log_symbols_basic_info(imported_symbols)

    process_string_symbols(imported_symbols)
    process_mem_symbols(imported_symbols)


PluginCommand.register("Basic information",
                       "Logs basic information of the binary",
                       find_vulns)
