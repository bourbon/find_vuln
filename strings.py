from functools import partial
from typing import Iterable

from binaryninja import log_info, Symbol

from . import bv
from .util import get_symbol_info_from_func, hexify, is_special_call


INTERESTING_STR_CALLS = 'read', 'get', 'put', 'print', 'str'


def process_string_symbols(symbols: Iterable[Symbol]):
    is_str_symbol: function = partial(is_special_call, calls=INTERESTING_STR_CALLS)
    rel_symbols = list(filter(is_str_symbol, symbols))

    lines = []
    relevant_funcs = {caller.function for symbol in rel_symbols for caller in bv.get_callers(symbol.address)}
    for func in sorted(relevant_funcs, key=lambda f: f.start):
        lines.append(f' - Function {hexify(func.start)} {func.name}')
        lines.extend(get_symbol_info_from_func(func, rel_symbols))

    log_info("Interesting string calls:\n%s" % '\n'.join(lines))
