from functools import partial
from typing import Iterable

from binaryninja import log_info, Function, Symbol

from . import bv
from .util import hexify, is_special_call, get_symbol_info_from_func


INTERESTING_MEM_CALLS = 'memcpy', 'free', 'alloc'


def process_mem_symbols(symbols: Iterable[Symbol]):
    is_mem_symbol: function = partial(is_special_call, calls=INTERESTING_MEM_CALLS)
    rel_symbols = list(filter(is_mem_symbol, symbols))

    lines = []
    relevant_funcs = {caller.function for symbol in rel_symbols for caller in bv.get_callers(symbol.address)}
    for func in sorted(relevant_funcs, key=lambda f: f.start):
        vuln_status = check_memory_vulnerability(func)

        lines.append(f' - Function {hexify(func.start)} {func.name} {vuln_status}')
        lines.extend(get_symbol_info_from_func(func, rel_symbols))

    log_info("Interesting memory calls:\n%s" % '\n'.join(lines))


def check_memory_vulnerability(func: Function):
    return ''
