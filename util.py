from typing import AnyStr, Iterable

from binaryninja import Function, Symbol, SymbolType, log_info

from . import bv


MAX_SYMBOL_TYPE_LEN = max([len(e.name) for e in SymbolType])


def hexify(i: int, pad: int = 8):
    return f'0x{i:0{pad}}'


def log_symbols_basic_info(symbols: Iterable[Symbol]):
    lines = [f' - {symbol.type.name:{MAX_SYMBOL_TYPE_LEN}} {hexify(symbol.address)} {symbol.name}'
             for symbol in symbols]
    log_info("Imported symbols:\n%s" % '\n'.join(lines))


def get_symbol_info_from_func(func: Function, callees: Iterable[Symbol]):
    out = []
    for callee in callees:
        local_refs = bv.get_code_refs_from(callee.address, func=func)
        hex_refs = ', '.join(map(hexify, local_refs))
        out.append(f'   + {hexify(callee.address)} {callee.name} called at {hex_refs}')

    return out


def get_relevant_callees(func: Function, symbols: Iterable[Symbol]):
    rel_calls = {callee.symbol for callee in func.callees if callee.symbol in symbols}
    return sorted(rel_calls, key=lambda sym: sym.address)


def is_special_call(sym: Symbol, call_names: Iterable[AnyStr]):
    for c in call_names:
        if c in sym.name:
            return True

    return False
